#!/usr/bin/lua
local _FILENAME = "skv_srv.lua"
local _AUTHOR = "marco (@macarony.de - Matthias Diener)"
local _VERSION = "20101125"

require("tcp_srv")

local t_data = {}
local t_tmp_data = {}
setmetatable(t_tmp_data, {__index = function(table, key) return t_data[key]; end})
local s_data = nil

--[[
[Prompt]Input

  []+key
  [>]content
  [.]
  []?
  key
  []=key
  content
  []*key2
  [>]content2
  [.]
  []?
  key
  []=key2
  content2
  []#
  [.]
  []=key2

  []=key
  content
  []-key
  [.]
  []?

--]]

local kv_callback = function(is_data)
  if s_data then
    if s_data.s_cmd == "+" then
      t_data[s_data.s_key] = is_data
      t_tmp_data[s_data.s_key] = nil
    elseif s_data.s_cmd == "*" then
      t_data[s_data.s_key] = nil
      t_tmp_data[s_data.s_key] = is_data
    end
    s_data = nil
    return ".\r\n"
  end
  local s_cmd = string.sub(is_data, 1, 1)
  if (s_cmd == "+") or (s_cmd == "*") then
    s_data = {s_cmd = s_cmd, s_key = string.sub(is_data, 2)}
    return ">"
  elseif s_cmd == "-" then
    t_data[string.sub(is_data, 2)] = nil
    t_tmp_data[string.sub(is_data, 2)] = nil
    return ".\r\n"
  elseif s_cmd == "#" then
    t_tmp_data = {}
    setmetatable(t_tmp_data, {__index = function(table, key) return t_data[key]; end})
    return ".\r\n"
  elseif s_cmd == "?" then
    local t_keys = {}
    for k, _ in pairs(t_data) do
      t_keys[(#t_keys + 1)] = k
    end
    return string.format("%s\r\n", table.concat(t_keys, "\r\n"))
  elseif s_cmd == "=" then
    return string.format("%s\r\n", t_tmp_data[string.sub(is_data, 2) or ""] or "")
  end
end

tcp_srv.run("127.0.0.1", 1234, kv_callback)


