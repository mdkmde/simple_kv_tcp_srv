#!/usr/bin/lua
local _FILENAME = "tcp_srv.lua"
local _AUTHOR = "marco (@macarony.de - Matthias Diener)"
local _VERSION = "20101125"

local pairs = pairs
local coroutine = {create = coroutine.create, resume = coroutine.resume, yield = coroutine.yield}
local socket = require("socket")
local SHUTDOWN_CMD = "SHUTDOWN"

module(...)
getVersion = function() return _VERSION, _AUTHOR, _FILENAME; end

local echo_callback = function(is_data)
  return is_data .. "\r\n"
end

local tcp = socket.tcp()

local receive = function(connection)
  connection:settimeout(0, "b")
  while true do
    local s_data, s_status = connection:receive("*l")
    if s_status == "timeout" then
      coroutine.yield(connection)
    else
      return s_data, s_status
    end
  end
end

run = function(is_host, in_port, i_data_callback, is_shutdown_cmd)
  if not(tcp:bind(is_host or "*", in_port or 7)) then
    return "Cannot bind"
  end
  i_data_callback = i_data_callback or echo_callback
  is_shutdown_cmd = is_shutdown_cmd or SHUTDOWN_CMD
  if not(tcp:listen()) then
    return "Cannot listen"
  end
  tcp:setoption("reuseaddr", true)
  tcp:setoption("tcp-nodelay", true)
  tcp:settimeout(0, "b")
  local t_threads = {}
  local t_connections = {tcp}
  local running = true;
  while running do
    local t_ready = socket.select(t_connections)
    for i = 1, #t_ready do
      if t_ready[i] == tcp then
        local connection = tcp:accept()
        if connection then
          t_connections[(#t_connections + 1)] = connection
          local thread = coroutine.create(function()
            while true do
              local s_data, s_status = receive(connection)
              if s_status == "closed" then
                break
              elseif s_data == is_shutdown_cmd then
                connection:shutdown()
                running = false
                break
              end
              connection:send(i_data_callback(s_data) or "")
            end
            for k, v in pairs(t_connections) do
              if v == connection then
                t_connections[k] = t_connections[#t_connections]
                t_connections[#t_connections] = nil
                break
              end
            end
            connection:close()
          end)
          t_threads[connection] = thread
        end
      else
        local _, connection = coroutine.resume(t_threads[t_ready[i]])
        if not(connection) then
          t_threads[t_ready[i]] = nil
        end
      end
    end
  end
  -- SHUTDOWN / cleanup server
  t_threads = nil
  for _, connection in pairs(t_connections) do
    if not(connection == tcp) then
      connection:shutdown()
      connection:close()
    end
  end
  t_connections = nil
  tcp:close()
  tcp = nil
  return is_shutdown_cmd
end

